<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Library of interface functions and constants.
 * @package     mod_langodemo
 * @copyright   2018 lango
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->libdir . '/eventslib.php');
/**
 * Return if the plugin supports $feature.
 * @param string $feature Constant representing the feature.
 * @return true | null True if the feature is supported, null otherwise.
 */
function mod_langodemo_supports($feature) {
    switch ($feature) {
        case FEATURE_MOD_INTRO:
            return true;
        default:
            return null;
    }
}

/**
 * Saves a new instance of the mod_langodemo into the database.
 * Given an object containing all the necessary data, (defined by the form
 * in mod_form.php) this function will create a new instance and return the id
 * number of the instance.
 * @param object $moduleinstance An object from the form.
 * @param mod_langodemo_mod_form $mform The form.
 * @return int The id of the newly inserted record.
 */
function mod_langodemo_add_instance($moduleinstance, $mform = null) {
    global $DB;
    $moduleinstance->timecreated = time();
    $id                          = $DB->insert_record('mod_langodemo', $moduleinstance);

    return $id;
}

/**
 * Updates an instance of the mod_langodemo in the database.
 * Given an object containing all the necessary data (defined in mod_form.php),
 * this function will update an existing instance with new data.
 * @param object $moduleinstance An object from the form in mod_form.php.
 * @param mod_langodemo_mod_form $mform The form.
 * @return bool True if successful, false otherwise.
 */
function mod_langodemo_update_instance($moduleinstance, $mform = null) {
    global $DB;
    $moduleinstance->timemodified = time();
    $moduleinstance->id           = $moduleinstance->instance;

    return $DB->update_record('mod_langodemo', $moduleinstance);
}

/**
 * Removes an instance of the mod_langodemo from the database.
 * @param int $id Id of the module instance.
 * @return bool True if successful, false on failure.
 */
function mod_langodemo_delete_instance($id) {
    global $DB;
    $exists = $DB->get_record('mod_langodemo', array('id' => $id));
    if (!$exists) {
        return false;
    }
    $DB->delete_records('mod_langodemo', array('id' => $id));

    return true;
}

function lango_get_posts_by_user($user, $limitfrom = 0, $limitnum = 10) {
    global $DB, $USER, $CFG;
    $return                      = new stdClass;
    $return->totalcount          = 0;    // The total number of posts that the current user is able to view.
    $return->posts               = array();   // The posts to display.
    $userfields                  = user_picture::fields('u', null, 'useridx');
    $countsql                    = 'SELECT COUNT(*) ';
    $selectsql                   = 'SELECT p.*, ' . $userfields . ' ';
    $wheresql                    = '1=1';
    $sql                         = " FROM {langodemo_posts} p
            JOIN {user} u ON u.id = p.userid
           WHERE ($wheresql)
             AND p.userid = :userid ";
    $orderby                     = " ORDER BY p.lastmodified DESC ";
    $forumsearchparams['userid'] = $user->id;
    $return->totalcount          = $DB->count_records_sql($countsql . $sql, $forumsearchparams);
    $return->posts               = $DB->get_records_sql($selectsql . $sql . $orderby, $forumsearchparams, $limitfrom, $limitnum);

    return $return;
}

function lango_get_users_with_postcount($user, $limitfrom = 0, $limitnum = 10) {
    global $DB, $USER, $CFG;
    $return                      = new stdClass;
    $return->totalcount          = 0;    // The total number of posts that the current user is able to view.
    $return->users               = array();   // The posts to display.
    $countsql                    = 'SELECT COUNT(*) ';
    $selectsql                   = 'SELECT concat(u.firstname," ",u.lastname) as fullname,
u.id,l.lastmodified,
(select count(*) from {langodemo_posts} where userid=u.id) as total_posts ';
    $wheresql                    = '1=1';
    $sql                         = " FROM {user} u
    LEFT JOIN {langodemo_posts} as l on l.userid = u.id
           WHERE ($wheresql)";
    $orderby                     = " ORDER BY l.lastmodified DESC ";
    $forumsearchparams['userid'] = $user->id;
    $return->totalcount          = $DB->count_records_sql($countsql . $sql, $forumsearchparams);
    $return->users               = $DB->get_records_sql($selectsql . $sql . $orderby, $forumsearchparams, $limitfrom, $limitnum);

    return $return;
}

function lango_print_post($posts) {
    global $ADMIN, $CFG, $DB;
    $output = '';
    $output .= html_writer::start_tag('div',
                                      array('class' => 'row header'));
    $output .= html_writer::start_tag('table',
                                      array('class' => 'generaltable myclass'));

    $output .= html_writer::start_tag('thead');
    $output .= html_writer::start_tag('tr');

    $output .= html_writer::start_tag('th');
    $output .= 'Post Title';
    $output .= html_writer::end_tag('th');

    $output .= html_writer::start_tag('th');
    $output .= 'Post Description';
    $output .= html_writer::end_tag('th');

    $output .= html_writer::start_tag('th');
    $output .= 'Action';
    $output .= html_writer::end_tag('th');

    $output .= html_writer::end_tag('tr');
    $output .= html_writer::end_tag('thead');

    $output .= html_writer::start_tag('tbody');
    foreach ($posts as $post) {
        $posturl               = new moodle_url('/mod/langodemo/view.php',
                                                array('id' => $post->id));
        $postediturl           = new moodle_url('/mod/langodemo/edit.php',
                                                array('entryid' => $post->id, 'action' => 'edit'));
        $postdeleteurl         = new moodle_url('/mod/langodemo/edit.php',
                                                array('entryid' => $post->id, 'action' => 'delete'));
        $post->subjectnoformat = true;
        $output .= html_writer::start_tag('tr');

        $output .= html_writer::start_tag('td');
        $output .= $post->post_title;
        $output .= html_writer::end_tag('td');

        $output .= html_writer::start_tag('td');
        $output .= $post->post_description;
        $output .= html_writer::end_tag('td');

        $output .= html_writer::start_tag('td');
        $output .= html_writer::tag('a', '<i class="fa fa-eye"></i>',
                                    array('id' => 'p' . $post->id, 'href' => $posturl));
        if ($CFG->can_edit == 'Yes') {
            $output .= '&nbsp';
            $output .= html_writer::tag('a', '<i class="fa fa-pencil"></i>',
                                        array('id' => 'p' . $post->id, 'href' => $postediturl));
            $output .= '&nbsp';
            $output .= html_writer::tag('a', '<i class="fa fa-trash"></i>',
                                        array('id' => 'p' . $post->id, 'href' => $postdeleteurl));
        }
        $output .= html_writer::end_tag('td');

        $output .= html_writer::end_tag('tr');
    }
    $output .= html_writer::end_tag('tbody');

    $output .= html_writer::end_tag('table');
    $output .= html_writer::end_tag('div');

    return $output;
}

function lango_print_users($users) {
    global $ADMIN, $CFG, $DB;
    $output = '';
    $output .= html_writer::start_tag('div',
                                      array('class' => 'row header'));
    $output .= html_writer::start_tag('table',
                                      array('class' => 'generaltable myclass'));

    $output .= html_writer::start_tag('thead');
    $output .= html_writer::start_tag('tr');

    $output .= html_writer::start_tag('th');
    $output .= 'Name';
    $output .= html_writer::end_tag('th');

    $output .= html_writer::start_tag('th');
    $output .= 'Total Posts';
    $output .= html_writer::end_tag('th');

    $output .= html_writer::start_tag('th');
    $output .= 'Last Modified';
    $output .= html_writer::end_tag('th');

    $output .= html_writer::start_tag('th');
    $output .= 'Action';
    $output .= html_writer::end_tag('th');

    $output .= html_writer::end_tag('tr');
    $output .= html_writer::end_tag('thead');

    $output .= html_writer::start_tag('tbody');
    foreach ($users as $user) {
        $posturl               = new moodle_url('/mod/langodemo/posts.php',
                                                array('id' => $user->id));
        $post->subjectnoformat = true;
        $output .= html_writer::start_tag('tr');

        $output .= html_writer::start_tag('td');
        $output .= $user->fullname;
        $output .= html_writer::end_tag('td');

        $output .= html_writer::start_tag('td');
        $output .= $user->total_posts;
        $output .= html_writer::end_tag('td');

        $output .= html_writer::start_tag('td');
        $output .= (!empty($user->lastmodified)) ? gmdate("m-d-Y H:i:s", $user->lastmodified) : '-';
        $output .= html_writer::end_tag('td');

        $output .= html_writer::start_tag('td');
        $output .= html_writer::tag('a', 'View Posts',
                                    array('id' => 'p' . $user->id, 'href' => $posturl));
        $output .= html_writer::end_tag('td');

        $output .= html_writer::end_tag('tr');
    }
    $output .= html_writer::end_tag('tbody');

    $output .= html_writer::end_tag('table');
    $output .= html_writer::end_tag('div');

    return $output;
}

function lango_user_can_edit_entry($postentry) {
    global $USER;

    $sitecontext = context_system::instance();

    if (has_capability('moodle/langodemo:manageentries', $sitecontext)) {
        return true; // Can edit any post entry.
    }

    if ($postentry->userid == $USER->id && has_capability('moodle/langodemo:create', $sitecontext)) {
        return true; // Can edit own when having post:create capability.
    }

    return false;
}