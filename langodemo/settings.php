<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 * @package     mod_langodemo
 * @category    admin
 * @copyright   2018 lango
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {
    require_once($CFG->dirroot . '/mod/langodemo/lib.php');
    $settings->add(new admin_setting_configtext(
        'name',
        'Name',
        'Name for plugin',
        'Lango'));
    $settings->add(new admin_setting_configtextarea(
        'description',
        'Description',
        'Description for plugin',
        'Lango'));
    $settings->add(new admin_settings_num_course_sections(
        'max_number_of_post',
        'Max number of posts per student',
        'Max number of posts per student',
        1,
        10));
    $settings->add(new admin_settings_num_course_sections(
        'files_per_post',
        'Max number of files per post',
        'Max number of files per post',
        1,
        10));
    $settings->add(new admin_setting_configselect_with_advanced(
        'can_edit',
        'Can edit?',
        '',
        'Yes',
        ['Yes' => 'Yes', 'No' => 'No']));
}
