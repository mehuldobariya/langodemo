<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Display user activity reports for a course
 * @package   mod_langodemo
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../../config.php');

// Require valid moodle login.  Will redirect to login page if not logged in.
require_login();

require_once($CFG->dirroot . '/mod/langodemo/lib.php');
require_once($CFG->dirroot . '/user/lib.php');

$courseid  = optional_param('course', null, PARAM_INT); // Limit the posts to just this course.
$userid    = optional_param('id', 0, PARAM_INT);        // User id whose posts we want to view.
$userid    = ($userid == 0) ? optional_param('userid', 0, PARAM_INT) : $userid;

$getuserid = optional_param('id', 0, PARAM_INT);        // User id whose posts we want to view.
$getuserid = ($getuserid == 0) ? optional_param('userid', 0, PARAM_INT) : $getuserid;
$mode      = optional_param('mode', 'posts', PARAM_ALPHA);   // The mode to use. Either posts or discussions.
$page      = optional_param('page', 0, PARAM_INT);           // The page number to display.
$perpage   = optional_param('perpage', 10, PARAM_INT);     // The number of posts to display per page.

if (empty($userid) || $userid == 0) {
    if (!isloggedin()) {
        require_login();
    }
    $userid = $USER->id;
}

$user        = $DB->get_record("user", array("id" => $userid), '*', MUST_EXIST);
$usercontext = context_user::instance($user->id, MUST_EXIST);

$discussionsonly  = ($mode !== 'posts');
$isspecificcourse = !is_null($courseid);
$iscurrentuser    = ($USER->id == $userid);

$url = new moodle_url('/mod/langodemo/posts.php', array('id' => $userid));
$PAGE->set_url($url);
$PAGE->set_pagelayout('standard');

if ($page != 0) {
    $url->param('page', $page);
}
if ($perpage != 5) {
    $url->param('perpage', $perpage);
}
// Check if the requested user is the guest user.
if (isguestuser($user)) {
    // The guest user cannot post, so it is not possible to view any posts.
    // May as well just bail aggressively here.
    print_error('invaliduserid');
}
// Make sure the user has not been deleted.
if ($user->deleted) {
    $PAGE->set_title(get_string('userdeleted'));
    $PAGE->set_context(context_system::instance());
    echo $OUTPUT->header();
    echo $OUTPUT->heading($PAGE->title);
    echo $OUTPUT->footer();
    die;
}

$isloggedin      = isloggedin();
$isguestuser     = $isloggedin && isguestuser();
$isparent        = !$iscurrentuser && $DB->record_exists('role_assignments',
        array('userid' => $USER->id, 'contextid' => $usercontext->id));
$hasparentaccess = $isparent && has_all_capabilities(
        array('moodle/user:viewdetails', 'moodle/user:readuserposts'),
        $usercontext);

$roleassignments = $DB->get_records('role_assignments', ['userid' => $user->id]);
$roleassigned = '';
if (!empty($roleassignments)) {
    foreach ($roleassignments as $key => $value) {
        $roleassigned = $value->roleid;
    }
}
// Get the posts by the requested user that the current user can access.
if ($roleassigned == 5 && $userid > 0) {
    $result = lango_get_posts_by_user($user, ($page * $perpage), $perpage);
} else if ($getuserid > 0) {
    $result = lango_get_posts_by_user($user, ($page * $perpage), $perpage);
} else {
    $result = lango_get_users_with_postcount($user, ($page * $perpage), $perpage);
}
// Check whether there are not posts to display.
if (($roleassigned == 5 && $userid > 0) || $getuserid > 0) {
    if (empty($result->posts)) {
        // Ok no posts to display means that either the user has not posted;
        // Or there are no posts made by the requested user that the current user is able to see.
        // In either case we need to decide whether we can show personal information;
        // About the requested user to the current user so we will execute some checks.
        $canviewuser = user_can_view_profile($user, null, $usercontext);
        // Prepare the page title.
        $pagetitle = 'No posts';
        // Get the page heading.
        $pageheading = 'Lango';
        // Next we need to set up the loading of the navigation and choose a message;
        // To display to the current user.
        if ($iscurrentuser) {
            // No need to extend the navigation it happens automatically;
            // For the current user.
            $notification = get_string('nopostsmadebyyou', 'langodemo');
            $usernode     = $PAGE->navigation->find('users', null);
            $usernode->make_inactive();
            // Edit navbar.
            if (isset($courseid) && $courseid != SITEID) {
                // Create as much of the navbar automatically.
                $newusernode = $PAGE->navigation->find('user' . $user->id, null);
                $newusernode->make_active();
                if ($mode == 'posts') {
                    $navbar = $PAGE->navbar->add(get_string('posts', 'langodemo'), new moodle_url('/mod/langodemo/posts.php',
                        array('id' => $user->id, 'course' => $courseid)));
                }
            }
        } else if ($canviewuser) {
            $PAGE->navigation->extend_for_user($user);
            $PAGE->navigation->set_userid_for_parent_checks($user->id);
            // See MDL-25805 for reasons and for full commit reference for reversal when fixed.
            // Edit navbar.
            if (isset($courseid) && $courseid != SITEID) {
                // Create as much of the navbar automatically.
                $usernode = $PAGE->navigation->find('user' . $user->id, null);
                $usernode->make_active();
                if ($mode == 'posts') {
                    $navbar = $PAGE->navbar->add(get_string('posts', 'langodemo'), new moodle_url('/mod/langodemo/posts.php',
                        array('id' => $user->id, 'course' => $courseid)));
                }
            }

            $fullname     = fullname($user);
            $notification = get_string('nopostsmadebyuser', 'langodemo', $fullname);
        } else {
            // Don't extend the navigation it would be giving out information that the current uesr doesn't have access to.
            $notification = get_string('cannotviewusersposts', 'langodemo');
            if ($isspecificcourse) {
                $url = new moodle_url('/course/view.php', array('id' => $courseid));
            } else {
                $url = new moodle_url('/');
            }
            navigation_node::override_active_url($url);
        }

        // Display a page letting the user know that there's nothing to display.
        $PAGE->set_title($pagetitle);
        if ($canviewuser) {
            $PAGE->set_heading(fullname($user));
        } else {
            $PAGE->set_heading($SITE->fullname);
        }
        echo $OUTPUT->header();
        if (!$isspecificcourse) {
            $pagetitle .= '<div class="right pull-right">';
            $pagetitle .= '<a href="' . $CFG->wwwroot . '/mod/langodemo/edit.php?action=add' . '">Add new Post</a>';
            $pagetitle .= '</div>';
            echo $OUTPUT->heading($pagetitle);
        }
        echo $OUTPUT->notification($notification);
        if (!$url->compare($PAGE->url)) {
            echo $OUTPUT->continue_button($url);
        }
        echo $OUTPUT->footer();
        die;
    }
}

$postoutput = array();
if (($roleassigned == 5 && $userid > 0) || $getuserid > 0) {
    $postoutput[] = lango_print_post($result->posts);
} else {
    $postoutput[] = lango_print_users($result->users);
}

$userfullname = fullname($user);

if (($roleassigned == 5 && $getuserid > 0) || $getuserid > 0) {
    $inpageheading = get_string('postsmadebyuser', 'langodemo', $userfullname);
    if ($roleassigned == 5) {
        $inpageheading .= '<div class="right pull-right">';
        $inpageheading .= '<a href="' . $CFG->wwwroot . '/mod/langodemo/edit.php?action=add' . '">Add new Post</a>';
        $inpageheading .= '</div>';
    }
} else {
    $inpageheading = 'Users';
}
$pagetitle   = $inpageheading;
$pageheading = $userfullname;

$PAGE->set_title($pagetitle);
$PAGE->set_heading($pageheading);

$PAGE->navigation->extend_for_user($user);
// See MDL-25805 for reasons and for full commit reference for reversal when fixed.
$PAGE->navigation->set_userid_for_parent_checks($user->id);

echo $OUTPUT->header();
echo html_writer::start_tag('div', array('class' => 'user-content'));
echo $OUTPUT->heading($inpageheading);

if (!empty($postoutput)) {
    foreach ($postoutput as $post) {
        echo $post;
        echo html_writer::empty_tag('br');
    }
    echo $OUTPUT->paging_bar($result->totalcount, $page, $perpage, $url);
} else {
    $nopostheading = get_string('noposts', 'langodemo');
    echo $OUTPUT->heading($nopostheading);
}
echo html_writer::end_tag('div');
echo $OUTPUT->footer();
