<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Post entry edit page
 * @package    mod
 * @subpackage langodemo
 * @copyright  2018 ArsenalTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__ . '/../../config.php');
require_once($CFG->dirroot . '/mod/langodemo/locallib.php');
require_once($CFG->dirroot . '/mod/langodemo/lib.php');
require_once($CFG->dirroot . '/mod/langodemo/edit_form.php');

$action   = required_param('action', PARAM_ALPHA);
$id       = optional_param('entryid', 0, PARAM_INT);
$confirm  = optional_param('confirm', 0, PARAM_BOOL);

if ($action == 'edit') {
    $id = required_param('entryid', PARAM_INT);
}
$PAGE->set_url('/mod/langodemo/edit.php', array('action'  => $action,
                                                'entryid' => $id,
                                                'confirm' => $confirm));

// If action is add, we ignore $id to avoid any further problems.
if (!empty($id) && $action == 'add') {
    $id = null;
}

$entry     = new stdClass();
$entry->id = null;

if ($id) {
    if (!$entry = new post_entry($id)) {
        print_error('wrongentryid', 'blog');
    }
    $userid = $entry->userid;
} else {
    $userid = $USER->id;
}

$sitecontext = context_system::instance();

$usercontext = context_user::instance($userid);
require_login();
if (isguestuser()) {
    print_error('noguest');
}
$returnurl = new moodle_url('/mod/langodemo/posts.php');
if (!has_capability('moodle/blog:create', $sitecontext) && !has_capability('moodle/blog:manageentries', $sitecontext)) {
    print_error('cannoteditentryorblog');
}

// Make sure that the person trying to edit has access right.
if ($id) {
    $entry->post_title       = clean_text($entry->post_title);
    $entry->post_description = clean_text($entry->post_description, $entry->format);
}
$returnurl->param('userid', $userid);

// Blog renderer.
$output = $PAGE->get_renderer('blog');

$strblogs = get_string('posts', 'langodemo');

if ($action === 'delete') {
    // Init comment JS strings.
    comment::init();

    if (empty($entry->id)) {
        print_error('wrongentryid', 'blog');
    }
    if (data_submitted() && $confirm && confirm_sesskey()) {
        // Make sure the current user is the author of the blog entry, or has some deleteanyentry capability.
        if (!blog_user_can_edit_entry($entry)) {
            print_error('nopermissionstodeleteentry', 'blog');
        } else {
            $entry->delete();
            blog_rss_delete_file($userid);
            redirect($returnurl);
        }
    } else if (blog_user_can_edit_entry($entry)) {
        $optionsyes = array('entryid'  => $id,
                            'action'   => 'delete',
                            'confirm'  => 1,
                            'sesskey'  => sesskey());
        $optionsno  = array('userid' => $entry->userid);
        $PAGE->set_title("$SITE->shortname: $strblogs");
        $PAGE->set_heading($SITE->fullname);
        echo $OUTPUT->header();

        // Output edit mode title.
        echo $OUTPUT->heading($strblogs . ': ' . get_string('deleteentry', 'blog'), 2);

        echo $OUTPUT->confirm(get_string('blogdeleteconfirm', 'blog', format_string($entry->subject)),
            new moodle_url('edit.php', $optionsyes),
            new moodle_url('index.php', $optionsno));

        echo '<br />';
        // Output the entry.
        $entry->prepare_render();
        echo $output->render($entry);

        echo $OUTPUT->footer();
        die;
    }
} else if ($action == 'add') {
    $editmodetitle = $strblogs . ': ' . get_string('addnewentry', 'langodemo');
    $PAGE->set_title("$SITE->shortname: $editmodetitle");
    $PAGE->set_heading(fullname($USER));
} else if ($action == 'edit') {
    $editmodetitle = $strblogs . ': ' . get_string('editentry', 'langodemo');
    $PAGE->set_title("$SITE->shortname: $editmodetitle");
    $PAGE->set_heading(fullname($USER));
}
$summaryoptions    = array('maxfiles' => $CFG->files_per_post,
                           'maxbytes' => $CFG->maxbytes,
                           'trusttext' => true,
                           'context' => $sitecontext,
                           'subdirs'  => file_area_contains_subdirs($sitecontext,
                               'langodemo_post',
                               'post_description',
                               $entry->id));
$attachmentoptions = array('subdirs' => false,
                           'maxfiles' => $CFG->files_per_post,
                           'maxbytes' => $CFG->maxbytes);

$posteditform = new lango_edit_form(null, compact('entry',
    'summaryoptions',
    'attachmentoptions',
    'sitecontext'));

$entry = file_prepare_standard_editor($entry,
    'post_description',
    $summaryoptions,
    $sitecontext,
    'langodemo_post',
    'post_description',
    $entry->id);
$entry = file_prepare_standard_filemanager($entry,
    'attachment',
    $attachmentoptions,
    $sitecontext,
    'langodemo_post',
    'attachment',
    $entry->id);

$entry->action = $action;
// Set defaults.
$entry->post_description = array(
    'text' => $entry->post_description
);
$posteditform->set_data($entry);

if ($posteditform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $posteditform->get_data()) {
    switch ($action) {
        case 'add':
            $postentry = new post_entry(null, $data, $posteditform);
            $postentry->add();
            $postentry->edit($data, $posteditform, $summaryoptions, $attachmentoptions);
            break;
        case 'edit':
            if (empty($entry->id)) {
                print_error('wrongentryid', 'blog');
            }

            $entry->edit($data, $posteditform, $summaryoptions, $attachmentoptions);
            break;

        default :
            print_error('invalidaction');
    }

    redirect($returnurl);
}

// GUI setup.
switch ($action) {
    case 'add':
        // Prepare new empty form.
        $entry->publishstate = 'site';
        $strformheading      = get_string('addnewentry', 'blog');
        $entry->action       = $action;
        break;

    case 'edit':
        if (empty($entry->id)) {
            print_error('wrongentryid', 'blog');
        }
        $strformheading = get_string('updateentrywithid', 'blog');
        break;

    default :
        print_error('unknowaction');
}


echo $OUTPUT->header();
// Output title for editing mode.
if (isset($editmodetitle)) {
    echo $OUTPUT->heading($editmodetitle, 2);
}
$posteditform->display();
echo $OUTPUT->footer();
die;
