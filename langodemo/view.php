<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_langodemo.
 * @package     mod_langodemo
 * @copyright   2018 lango
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../../config.php');
require_once($CFG->dirroot . '/mod/langodemo/locallib.php');
require_once($CFG->dirroot . '/mod/langodemo/lib.php');
require_once($CFG->dirroot . '/mod/langodemo/edit_form.php');
$id            = optional_param('id', 0, PARAM_INT);
$entry     = new stdClass();
$entry->id = null;
if ($id) {
    if (!$entry = new post_entry($id)) {
        print_error('wrongentryid', 'blog');
    }
    $userid = $entry->userid;
} else {
    $userid = $USER->id;
}
$sitecontext = context_system::instance();
$usercontext = context_user::instance($userid);
require_login();
if (isguestuser()) {
    print_error('noguest');
}
$PAGE->set_url('/mod/langodemo/view.php', array('id' => $id));
$returnurl = new moodle_url('/mod/langodemo/posts.php');
if ($id) {
    $entry->post_title       = clean_text($entry->post_title);
    $entry->post_description = clean_text($entry->post_description, $entry->format);
}
$returnurl->param('userid', $userid);
// Post renderer.
$output = $PAGE->get_renderer('blog');
$strposts = get_string('posts', 'langodemo');
$viewmodetitle = $strposts . ': ' . get_string('viewentry', 'langodemo');
$PAGE->set_title("$SITE->shortname: $viewmodetitle");
$PAGE->set_heading(fullname($USER));
echo $OUTPUT->header();
// Output title for editing mode.
if (isset($viewmodetitle)) {
    echo $OUTPUT->heading($viewmodetitle, 2);
}
echo $entry->display($entry);
echo $OUTPUT->footer();
die;
