<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Classes for Posts.
 * @package    moodlecore
 * @subpackage post
 * @copyright  2009 Nicolas Connault
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/filelib.php');

/**
 * Blog_entry class. Represents an entry in a user's post. Contains all methods for managing this entry.
 * This class does not contain any HTML-generating code. See post_listing sub-classes for such code.
 * This class follows the Object Relational Mapping technique, its member variables being mapped to
 * the fields of the post table.
 * @package    moodlecore
 * @subpackage post
 * @copyright  2009 Nicolas Connault
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class post_entry implements renderable
{
    // Public Database fields.
    public $id;
    public $userid;
    public $subject;
    public $summary;
    public $rating = 0;
    public $attachment;

    // Locked Database fields (Don't touch these).
    public $module = 'post';
    public $content;
    public $format = 1;
    public $lastmodified;
    public $created;
    public $usermodified;

    // Other class variables.
    public $form;

    /** @var StdClass Data needed to render the entry */
    public $renderable;

    /**
     * Constructor. If given an id, will fetch the corresponding record from the DB.
     * @param mixed $idorparams A post entry id if INT, or data for a new entry if array
     */
    public function __construct($id = null, $params = null, $form = null) {
        global $DB, $PAGE, $CFG;

        if (!empty($id)) {
            $object = $DB->get_record('langodemo_posts', array('id' => $id));
            foreach ($object as $var => $val) {
                $this->$var = $val;
            }
        } else if (!empty($params) && (is_array($params) || is_object($params))) {
            foreach ($params as $var => $val) {
                $this->$var = $val;
            }
        }
        $this->form = $form;
    }
    /**
     * Gets the required data to print the entry
     */
    public function prepare_render() {
        global $DB, $CFG, $PAGE;
        $this->renderable       = new StdClass();
        $this->renderable->user = $DB->get_record('user', array('id' => $this->userid));
        $this->post_description = file_rewrite_pluginfile_urls($entry->post_description,
            'pluginfile.php',
            SYSCONTEXTID,
            'langodemo_post',
            'post_description',
            $this->id);
        // Entry attachments.
        $this->renderable->attachments = $this->get_attachments();
        $this->renderable->usercanedit = lango_user_can_edit_entry($this);
    }
    /**
     * Gets the entry attachments list
     * @return array List of post_entry_attachment instances
     */
    public function get_attachments() {

        global $CFG;

        require_once($CFG->libdir . '/filelib.php');

        $syscontext = context_system::instance();

        $fs    = get_file_storage();
        $files = $fs->get_area_files($syscontext->id, 'langodemo_post', 'attachment', $this->id);

        // Adding a post_entry_attachment for each non-directory file.
        $attachments = array();
        foreach ($files as $file) {
            if ($file->is_directory()) {
                continue;
            }
            $attachments[] = new lango_entry_attachment($file, $this->id);
        }

        return $attachments;
    }
    /**
     * Inserts this entry in the database. Access control checks must be done by calling code.
     * @param mform $form Used for attachments
     * @return void
     */
    public function process_attachment($form) {
        $this->form = $form;
    }
    /**
     * Inserts this entry in the database. Access control checks must be done by calling code.
     * TODO Set the publishstate correctly
     * @return void
     */
    public function add() {
        global $CFG, $USER, $DB;
        unset($this->id);
        $this->module           = 'post';
        $this->post_title       = (!empty($this->post_title)) ? $this->post_title : $_POST['post_description'];
        $this->post_description = (!empty($this->post_description)) ? $this->post_title : $_POST['post_description'];
        $this->userid           = (empty($this->userid)) ? $USER->id : $this->userid;
        $this->lastmodified     = time();
        $this->timecreated      = time();
        // Insert the new post entry.
        $this->id = $DB->insert_record('langodemo_posts', $this);
    }
    /**
     * Updates this entry in the database. Access control checks must be done by calling code.
     * @param array $params Entry parameters.
     * @param moodleform $form Used for attachments.
     * @param array $summaryoptions Summary options.
     * @param array $attachmentoptions Attachment options.
     * @return void
     */
    public function edit($params = array(), $form = null, $summaryoptions = array(), $attachmentoptions = array()) {
        global $CFG, $DB;
        $sitecontext = context_system::instance();
        $entry       = $this;

        $this->form = $form;
        foreach ($params as $var => $val) {
            $entry->$var = $val;
        }
        $entry                   = file_postupdate_standard_editor($entry,
            'post_description',
            $summaryoptions,
            $sitecontext,
            'langodemo_post',
            'post_description',
            $entry->id);
        $entry->post_description = $_POST['post_description']['text'];
        $entry                   = file_postupdate_standard_filemanager($entry,
            'attachment',
            $attachmentoptions,
            $sitecontext,
            'langodemo_post',
            'attachment',
            $entry->id);
        $entry->lastmodified     = time();
        // Update record.
        $DB->update_record('langodemo_posts', $entry);
    }
    /**
     * Deletes this entry from the database. Access control checks must be done by calling code.
     * @return void
     */
    public function delete() {
        global $DB;
        $this->delete_attachments();
        // Get record to pass onto the event.
        $record = $DB->get_record('langodemo_posts', array('id' => $this->id));
        $DB->delete_records('langodemo_posts', array('id' => $this->id));
    }
    /**
     * Deletes all the user files in the attachments area for an entry
     * @return void
     */
    public function delete_attachments() {
        $fs = get_file_storage();
        $fs->delete_area_files(SYSCONTEXTID, 'langodemo_post', 'attachment',
            $this->id);
        $fs->delete_area_files(SYSCONTEXTID, 'langodemo_post', 'post_description',
            $this->id);
    }

    public function display($entry) {
        $output = '';
        $output .= html_writer::start_tag('div',
            array('class' => 'row header'));
        $output .= html_writer::start_tag('div',
            array('class' => 'col-md-6'));

        $output .= html_writer::start_tag('div', array('class' => 'col-md-12'));
        $output .= html_writer::start_tag('label', array('class' => 'strong'));
        $output .= '<b>Post Title: </b>';
        $output .= html_writer::end_tag('label');
        $output .= html_writer::empty_tag('br');
        $output .= $entry->post_title;
        $output .= html_writer::end_tag('div');

        $output .= html_writer::empty_tag('br');
        $output .= html_writer::start_tag('div', array('class' => 'col-md-12'));
        $output .= html_writer::start_tag('label', array('class' => 'strong'));
        $output .= '<b>Post Description: </b>';
        $output .= html_writer::end_tag('label');
        $output .= $entry->post_description;
        $output .= html_writer::end_tag('div');

        $output .= html_writer::empty_tag('br');
        $output .= html_writer::start_tag('div', array('class' => 'col-md-12'));
        $output .= html_writer::start_tag('label', array('class' => 'strong'));
        $output .= '<b>Post Attachments: </b>';
        $output .= html_writer::end_tag('label');
        $sitecontext = context_system::instance();
        $out         = array();
        $fs          = get_file_storage();
        $files       = $fs->get_area_files($sitecontext->id, 'langodemo_post', 'attachment', $entry->id);
        $output .= html_writer::empty_tag('br');
        foreach ($files as $file) {
            if ($file->get_filename() != ".") {
                $filetype = $file->get_mimetype();
                if ($filetype == 'image/jpeg' || $filetype == 'image/png') {
                    $filename = '<i class="fa fa-image">&nbsp;&nbsp;' . $file->get_filename() . '</i>';
                } else {
                    $filename = '<i class="fa fa-file">&nbsp;&nbsp;' . $file->get_filename() . '</i>';
                }
                $url   = moodle_url::make_file_url('/pluginfile.php', array($file->get_contextid(), 'langodemo_post', 'attachment',
                    $file->get_itemid(), $file->get_filepath(), $filename));
                $out[] = html_writer::link($url, $filename);
            }
        }
        $br = html_writer::empty_tag('br');
        $output .= implode($br, $out);
        $output .= html_writer::end_tag('div');

        $output .= html_writer::end_tag('div');
        $output .= html_writer::end_tag('div');
        return $output;
    }
    /**
     * User can edit a post entry if this is their own post entry and they have
     * the capability moodle/post:create, or if they have the capability
     * moodle/post:manageentries.
     * This also applies to deleting of entries.
     * @param int $userid Optional. If not given, $USER is used
     * @return boolean
     */
    public function can_user_edit($userid = null) {
        global $CFG, $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        $sitecontext = context_system::instance();

        if (has_capability('moodle/post:manageentries', $sitecontext)) {
            return true; // Can edit any post entry.
        }

        if ($this->userid == $userid && has_capability('moodle/post:create', $sitecontext)) {
            return true; // Can edit own when having post:create capability.
        }

        return false;
    }
    /**
     * Checks to see if a user can view the Posts of another user.
     * Only post level is checked here, the capabilities are enforced
     * in post/index.php
     * @param int $targetuserid ID of the user we are checking
     * @return bool
     */
    public function can_user_view($targetuserid) {
        global $CFG, $USER, $DB;
        $sitecontext = context_system::instance();

        if (empty($CFG->enableposts) || !has_capability('moodle/post:view', $sitecontext)) {
            return false; // Blog system disabled or user has no post view capability.
        }

        if (isloggedin() && $USER->id == $targetuserid) {
            return true; // Can view own entries in any case.
        }

        if (has_capability('moodle/post:manageentries', $sitecontext)) {
            return true; // Can manage all entries.
        }

        // Coming for 1 entry, make sure it's not a draft.
        if ($this->publishstate == 'draft' && !has_capability('moodle/post:viewdrafts', $sitecontext)) {
            return false;  // Can not view draft of others.
        }

        // Coming for 1 entry, make sure user is logged in, if not a public post.
        if ($this->publishstate != 'public' && !isloggedin()) {
            return false;
        }

        switch ($CFG->postlevel) {
            case BLOG_GLOBAL_LEVEL:
                return true;
                break;

            case BLOG_SITE_LEVEL:
                if (isloggedin()) { // Not logged in viewers forbidden.
                    return true;
                }

                return false;
                break;

            case BLOG_USER_LEVEL:
            default:
                $personalcontext = context_user::instance($targetuserid);

                return has_capability('moodle/user:readuserposts', $personalcontext);
                break;
        }
    }
}

class lango_entry_attachment implements renderable
{

    public $filename;
    public $url;
    public $file;

    /**
     * Gets the file data
     * @param stored_file $file
     * @param int $entryid Attachment entry id
     */
    public function __construct($file, $entryid) {

        global $CFG;

        $this->file     = $file;
        $this->filename = $file->get_filename();
        $this->url      = file_encode_url($CFG->wwwroot . '/pluginfile.php',
            '/' . SYSCONTEXTID . '/langodemo_post/attachment/' . $entryid . '/' . $this->filename);
    }

}
