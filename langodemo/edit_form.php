<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');
    // It must be included from a Moodle page.
}

require_once($CFG->libdir . '/formslib.php');

class lango_edit_form extends moodleform
{
    public $modnames = array();

    /**
     * lango form definition.
     */
    public function definition() {
        global $CFG, $DB;
        $mform                         =& $this->_form;
        $entry                         = $this->_customdata['entry'];
        $modid                         = $this->_customdata['modid'];
        $summaryoptions                = $this->_customdata['summaryoptions'];
        $attachmentoptions             = $this->_customdata['attachmentoptions'];
        $attachmentoptions['maxfiles'] = $CFG->files_per_post;
        $attachmentoptions['accepted_types'] = '*.jpg, *.jpeg, *.png,".ppt", ".pptx", ".doc", ".docx", ".xls", ".xlsx"';
        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'post_title', get_string('entrytitle', 'langodemo'), array('size' => 60, 'maxlength' => 128));
        $mform->addElement('editor', 'post_description', get_string('entrybody', 'langodemo'), null, $summaryoptions);

        $mform->setType('post_title', PARAM_TEXT);
        $mform->addRule('post_title', get_string('emptytitle', 'langodemo'), 'required', null, 'client');
        $mform->addRule('post_title', get_string('maximumchars', '', 128), 'maxlength', 128, 'client');

        $mform->setType('post_description', PARAM_RAW);
        $mform->addRule('post_description', get_string('emptybody', 'langodemo'), 'required', null, 'client');

        $mform->addElement('filemanager', 'attachment_filemanager', get_string('attachment', 'forum'), null, $attachmentoptions);

        $this->add_action_buttons();
        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_ALPHANUMEXT);
        $mform->setDefault('action', '');

        $mform->addElement('hidden', 'entryid');
        $mform->setType('entryid', PARAM_INT);
        $mform->setDefault('entryid', $entry->id);
    }

    /**
     * Validate the lango form data.
     * @param array $data Data to be validated
     * @param array $files unused
     * @return array|bool
     */
    public function validation($data, $files) {
        global $CFG, $DB, $USER;
        $errors = parent::validation($data, $files);
        if ($errors) {
            return $errors;
        }

        return true;
    }
}
