<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Plugin strings are defined here.
 *
 * @package     mod_langodemo
 * @category    string
 * @copyright   2018 lango
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();
$string['pluginname']                  = 'Lango';
$string['userpreferences']             = 'User preferences';
$string['myposts']                     = 'My Posts';
$string['nodiscussionsstartedbyyou']   = 'You haven\'t started any discussions yet';
$string['nopostsmadebyyou']            = 'You haven\'t made any posts';
$string['posts']                       = 'Posts';
$string['cannotviewusersposts']        = 'There are no posts made by this user that you are able to view.';
$string['postsmadebyuser']             = 'Posts made by {$a}';
$string['postsmadebyuserincourse']     = 'Posts made by {$a->fullname} in {$a->coursename}';
$string['nopostsmadebyuser']           = '{$a} has made no posts';
$string['addnewentry']                 = 'Add a new entry';
$string['entrytitle']                  = 'Entry title';
$string['entrysaved']                  = 'Your entry has been saved';
$string['entrybody']                   = 'Post Description';
$string['entrybodyonlydesc']           = 'Post Description';
$string['emptyurl']                    = 'You must specify a URL to a valid RSS feed';
$string['emptytitle']                  = 'Post title can not be empty';
$string['emptybody']                   = 'Post body can not be empty';
$string['privacy:metadata:core_files'] = 'Files attached to Post';
$string['addnewentry'] = 'Add a new entry';
$string['editentry'] = 'Edit a post entry';
$string['viewentry'] = 'View a post entry';
